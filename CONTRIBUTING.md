Thanks for your interest in helping to improve this project!

Some ideas on how you can help depending on how much time you have to spend:

- **20 min**
    - [#59](https://gitlab.com/stemcellbioengineering/context-explorer/issues/59)
      Install the software and provide feedback on the experience.
- **40-60 min**
    - [#62](https://gitlab.com/stemcellbioengineering/context-explorer/issues/62)
      Use the sample data set to follow analysis instructions and provide
      feedback on the experience.
    - [#53](https://gitlab.com/stemcellbioengineering/context-explorer/issues/53) Read the documentation and provide feedback for improvements.
    - General feedback and suggestions ([open a new issue](https://gitlab.com/stemcellbioengineering/context-explorer/issues/new) or reach out via the
      [Gitter chat](https://gitter.im/context_explorer/Lobby))
- **60+ min**
    - [#60](https://gitlab.com/stemcellbioengineering/context-explorer/issues/60) Code improvements
    - [#63](https://gitlab.com/stemcellbioengineering/context-explorer/issues/63) Help setting up tests and continuous integrations
    - Improving existing features (e.g.
      [#47](https://gitlab.com/stemcellbioengineering/context-explorer/issues/47))
    - Implementing new features

The best way to get in touch to discuss new ideas is to [open a new
issue](https://gitlab.com/stemcellbioengineering/context-explorer/issues/new) or
reach out via the [Gitter chat](https://gitter.im/context_explorer/Lobby).
If you additional information about what we need help with, you can look at
the issue list and keep an eye out for items labelled as
[good-first-issue](https://gitlab.com/stemcellbioengineering/context-explorer/issues?label_name%5B%5D=good-first-issue).
Remember to follow the [Code of
conduct](https://gitlab.com/stemcellbioengineering/context-explorer/blob/master/CODE_OF_CONDUCT.md)
when you participate in this project =)
